import boto3
import json
import os

comprehend = boto3.client(service_name='comprehend')

def analyse_sentiment(text):
    data = comprehend.detect_sentiment(Text=text, LanguageCode='en')
    return data